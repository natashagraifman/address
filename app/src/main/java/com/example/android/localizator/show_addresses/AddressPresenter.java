package com.example.android.localizator.show_addresses;

import android.app.Activity;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Natasha on 08/11/2017.
 */
 //Classe responsavel por verificar o valor inserido e fazer a inserção na lista de endereços
public class AddressPresenter {
    private final int RC_ADD_ADDRESS = 123;
    private AddressView addressView;
    private List<String> addressList = new ArrayList<>();

    AddressPresenter(AddressView addressView){
        this.addressView = addressView;
    }

    void addMovieInList(int requestCode, int resultCode, Intent data){
        if(requestCode == RC_ADD_ADDRESS && resultCode == Activity.RESULT_OK) {
            addressList.add(data.getStringExtra("movie_name"));
            addressView.updateList(addressList);

        }

     }
    }