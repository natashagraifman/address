package com.example.android.localizator.add_address;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.android.localizator.R;

public class AddAddressActivity extends AppCompatActivity {

    // FAZENDO BINDVIEW
    @BindView(R.id.edt_address)
    TextView edtAddress;
    @BindView(R.id.btn_add)
    Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);

        ButterKnife.bind(this); //  INICIALIZANDO A BIBLIOTECA

        //edtAddress = findViewById(R.id.edt_address); FOI FEITO BINDVIEW COM BUTTERKNIFE
        //btnAdd = findViewById(R.id.btn_add);

        /*
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //verifica se há um endereço digitado
                if (edtAddress.getText().toString().isEmpty()){
                    Toast.makeText(AddAddressActivity.this, "Digite o endereço que deseja adicionar", Toast.LENGTH_SHORT).show();
                }else {
                    //retorna o endereço para a MainActivity
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("movie_name", edtAddress.getText().toString());
                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();
                }
            }
        }); */
    }

    // USANDO ONCLICK DO BUTTERKNIFE
    @OnClick(R.id.btn_add)
    public void validaEndereco(){
        if (edtAddress.getText().toString().isEmpty()){
            Toast.makeText(AddAddressActivity.this, "Digite o endereço que deseja adicionar", Toast.LENGTH_SHORT).show();
        }else {
            //retorna o endereço para a MainActivity
            Intent resultIntent = new Intent();
            resultIntent.putExtra("movie_name", edtAddress.getText().toString());
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        }

    }


}
