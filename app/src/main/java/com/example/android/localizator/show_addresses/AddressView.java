package com.example.android.localizator.show_addresses;

import java.util.List;

/**
 * Created by Natasha on 08/11/2017.
 */
//Realiza a update da lista através de uma interface
 interface AddressView {
    void updateList(List<String> addressesList);
}
