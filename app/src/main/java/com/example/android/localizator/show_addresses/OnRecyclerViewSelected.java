package com.example.android.localizator.show_addresses;

import android.view.View;

/**
 * Created by Natasha on 08/11/2017.
 */

public interface OnRecyclerViewSelected {
    void onClick(View view, int position);
}
