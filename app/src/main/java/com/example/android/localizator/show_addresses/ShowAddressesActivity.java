package com.example.android.localizator.show_addresses;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.android.localizator.R;
import com.example.android.localizator.add_address.AddAddressActivity;
import com.example.android.localizator.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
//Nessa classe nos tiramos da main activity a verificacao e colocamos aqui. Alem disso, a adição dos itens na lista é feita
//através do AddressPresenter, respeitando os conceitos de MVP
public class ShowAddressesActivity extends AppCompatActivity implements AddressView{

    //FAZENDO BINDVIEW COM BUTTERKNIFE
    @BindView(R.id.rv_addresses)
    RecyclerView rvAddresses;
    private final int RC_ADD_ADDRESS = 123;
    private ArrayList<String> lstAddresses = new ArrayList<>();

    AddressPresenter addressPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_addresses);
        ButterKnife.bind(this);

        addressPresenter = new AddressPresenter(this);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        addressPresenter.addMovieInList(requestCode, resultCode,data);
        //captura o resultado da tela de cadastro de endereços e adiciona na lista
    }


    @Override
    public void updateList(List<String> addressesList) {
        AddressesAdapter addressesAdapter = new AddressesAdapter(lstAddresses);
        lstAddresses = getIntent().getStringArrayListExtra("addresses_list");
        addressesAdapter.setOnRecyclerViewSelected(new OnRecyclerViewSelected() {
            @Override
            public void onClick(View view, int position) {

                Intent intentMapa = new Intent(Intent.ACTION_VIEW);
                intentMapa.setData(Uri.parse("geo:0,0?q=" + rvAddresses));
                if(intentMapa.resolveActivity(getPackageManager()) != null) {
                    startActivity(intentMapa);
                }else {
                    Toast toast = Toast.makeText(ShowAddressesActivity.this, "Impossível abrir o recurso", Toast.LENGTH_LONG);
                    toast.show();
                }

            }

    });

    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvAddresses.setLayoutManager(layoutManager);

        rvAddresses.setAdapter(addressesAdapter);
    }
}